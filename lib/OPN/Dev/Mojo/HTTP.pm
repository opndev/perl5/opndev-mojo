package OPN::Dev::Mojo::HTTP;
our $VERSION = "0.01";
use Mojo::Base 'Mojolicious';

use MojoX::Log::Log4perl;
use Log::Log4perl::Level qw();

# ABSTRACT: OPN Dev Mojo HTTP service

sub _init_logger {
    my $self = shift;

    $self->log(
        MojoX::Log::Log4perl->new(
            $self->config->{log4perl}
            ? (
                $self->config->{log4perl}{conf},
                $self->config->{log4perl}{delay}
                )
            : (),
        )
    );

    my $root  = $self->log->_get_logger(0);
    my $level = Log::Log4perl::Level::to_level($root->level);

    $self->config->{log_level} = lc($level);

    return 1;
}

sub _get_filename {
    my ($self, $name) = @_;
    return $self->home->rel_file($name);
}

sub _init_openapi {
    my $self = shift;

    return unless $self->config->{openapi};
    my $config = $self->config;

    my %args
        = map { $_ => $config->{openapi}{$_} }
        grep  { $config->{openapi}{$_} }
        qw(
        add_preflighted_routes
        log_level
        schema
        plugins
        url
    );

    unless (exists $args{url}) {
        $args{url} = $self->_get_filename($config->{openapi}{spec}),;
    }

    $self->plugin(
        "OpenAPI" => {
            add_preflighted_routes => 1,
            schema                 => 'v3',
            %args,
        }
    );


    if (my $cors = $config->{openapi}{cors}) {
        $self->defaults(
            openapi_cors_default_max_age => $cors->{default_max_age} // 1);

        if (my $origins = $cors->{allowed_origins}) {
            my @qr = map { qr/$_/ } @{$origins};
            $self->defaults(openapi_cors_allowed_origins => \@qr);
        }
    }

}

sub _init_secrets {
    my $self = shift;
    $self->secrets($self->config->{secrets});
    return 1;
}

# This method will run once at server start
sub startup {
    my $self = shift;

    if ($self->can('init_config')) {
        $self->init_config;
    }
    else {
        $self->plugin('Config' => { file => 'etc/app.conf' });
    }
    $self->_init_secrets;
    $self->_init_logger;
    $self->_init_openapi;

}

1;
